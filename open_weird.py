import os
import xmltodict 
import fileinput
import sys 
import glob

test_path = '/Users/AlexSoong/desktop/BalladsRDF/event/'
# test_file = '/Users/AlexSoong/desktop/BalladsRDF/event/00/TEST.xml'

# os.chdir(test_path)
# df = open(test_file, 'rw') 
# print df 
test_file = '/Users/AlexSoong/desktop/BalladsRDF/test_event/event/00/0014d521-5836-4463-acbc-e442ad719f43.rdf'
def fix_bad_tag(file_path):
  correct = '</ballads:creationDateText>'
  incorrect = '</dcterms:creationDateText>'
  for i, line in enumerate(fileinput.input(file_path, inplace = 1)):
    sys.stdout.write(line.replace(incorrect, correct))

# if __name__ == '__main__':
# fix_bad_tag(test_file)
# with open(test_file) as df:
#   doc = xmltodict.parse(df.read())

# os.chdir(test_path)
# files = glob.glob('*.rdf')
# for file in files:
#   fix_bad_tag(file)

if __name__ == '__main__':
  not_ok_count = 0
  os.chdir(test_path)
  # get all directories
  dirs = glob.glob('*')
  for dir in dirs:
    # for each dir, switch to dir, and get all files
    os.chdir(test_path + dir)
    files = glob.glob('*.rdf')
    for file in files:
      # for each file, pass file path to fix bad tag
      file_path = test_path + dir + '/' +file
      # print file_path
      fix_bad_tag(file_path)
      # try to parse it with xmltodict
      with open(file) as df:
        doc = xmltodict.parse(df.read())


      # print str(test_path+dir+file) # path to the test file 
      # try:
      #   with open(file) as df:
      #     doc = xmltodict.parse(df.read())
      # except:
      #   print 'Not OK'

        # bad_file = test_path + dir + file
        # fix_bad_tag(bad_file)


  # print not_ok_count

