import os 
import untangle
import glob
import xmltodict

'''
for a sheet item in a sheetitem directory returns :
  1. sheet's origin url 
  2. sheet type url
  3. sheet's sheet manifeestation url, and sheetmanifestation number 
  4. sheet's event url, and event number
  5. sheet's image url, and image id 
  6. sheet's shelfMark 
  7. sheet's ballad identifier UUID
'''
sheetitem_path = "/Users/AlexSoong/Desktop/BalladsRDF/sheetitem/0"
os.chdir(sheetitem_path)
sheetitem_files = glob.glob('*')

with open(sheetitem_files[0]) as df:
  doc = xmltodict.parse(df.read())

# print doc

# print sheet_origin_url
sheet_description = doc['rdf:RDF']['rdf:Description']
sheet_origin_url = list(sheet_description.items())[0][-1]


# sheet type url 
sheet_type = doc['rdf:RDF']['rdf:Description']['rdf:type']
sheet_type_items = list(sheet_type.items())
sheet_type_url = sheet_type_items[0][-1]

# from sheet manifestation directory
# sheet exemplarOf, sheetmanifestation url, and sheet manifestation number
sheet_exemplarOf = doc['rdf:RDF']['rdf:Description']['ballads:exemplarOf']
sheet_exemplarOf_url = list(sheet_exemplarOf.items())[0][-1]
sheet_sheetmanifestation = str.split(str(sheet_exemplarOf_url), '/')[-1]
# print sheet_sheetmanifestation


# get sheet event url, event uuid, from event directory
sheet_collection = doc['rdf:RDF']['rdf:Description']['ballads:partOfCollection']
sheet_event_url = list(sheet_collection.items())[0][-1]
sheet_event_UUID = str.split(str(sheet_event_url),'/')[-1]
# print sheet_event_url, sheet_event_UUID


# sheet image url, and image id from sheetitemimage directory
sheet_image = doc['rdf:RDF']['rdf:Description']['ballads:hasDigitalImage']
sheet_image_url = list(sheet_image.items())[0][-1]
sheet_image_id = str.split(str(sheet_image_url), '/')[-1]
# print sheet_image_id
# print sheet_image_url


# ballads shelfMark
sheet_shelfMark = doc['rdf:RDF']['rdf:Description']['ballads:shelfMark']
# print sheet_shelfMark

# ballads UUID
sheet_UUID = str.split(str(doc['rdf:RDF']['rdf:Description']['ballads:identifier']), ' ')[-1]
print sheet_UUID










