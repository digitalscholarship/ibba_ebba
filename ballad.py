# modules 
# import collection
import event 
import sheetitem
import sheetmanifestation
import element
# import image 
# import balladtextmanifestation
# import element 
import MySQLdb
import pickle

# def db_insert(ballad):

def db_connect():
  db = MySQLdb.connect(host="localhost",
                        user="root",
                        passwd="alexDSI123",
                        db="ibba_test")

  print 'DB READY FOR INSERT'
  return db



# main 
if __name__ == '__main__':

  class ibba_ballad():
    ''' 
    the object that im going to insert into ibba_ballads table
    going to have a list of these objects
    iterate and insert into mysql 
    ''' 
    def __init__(self,
                  # BD_ID, AUTOINCREMENTING, DONT NEED TO INCLUDE
                  ARCH_ID,
                  ESTCID,
                  Shelfmark,
                  Imprint,
                  License,
                  AuthorImprint, 
                  PubStartDate,
                  PubEndDate,
                  StandardPrintPub,
                  CollectionInfo,
                  URL):
      # self.BD_ID = BD_ID
      self.ARCH_ID = ARCH_ID
      self.ESTCID = ESTCID
      self.Shelfmark = Shelfmark
      self.Imprint = Imprint
      self.License = License
      self.AuthorImprint = AuthorImprint
      self.PubStartDate = PubStartDate
      self.PubEndDate = PubEndDate
      self.StandardPrintPub = StandardPrintPub
      self.CollectionInfo = CollectionInfo
      self.URL = URL

  # get all the information from separate modules
  # print 'LOADING DATA FROM SHEET_MANIFESTATIONS...'
  # sheet_manifs = sheetmanifestation.get_all()
  # print 'LOADING DATA FROM SHEET_ITEMS...'
  # sheet_items = sheetitem.get_all()
  # print 'LOADING DATA FROM ELEMENTS...'
  # elements = element.get_all()
  # print 'LOADING DATA FROM EVENTS...'
  # events = event.get_all()
  # print 'FINISHED LOADING DATA FROM MODULES...'

  # load pickled files 
  sheet_manifs = pickle.load(open('./pickled/sheets.p', 'rb'))
  sheet_items = pickle.load(open('./pickled/sheetitems.p', 'rb'))
  elements = pickle.load(open('./pickled/elements.p', 'rb'))
  events = pickle.load(open('./pickled/events.p', 'rb'))

  # total number of estcid 
  TOTAL_NUM_SHEETMANIFS = len(sheet_manifs)
  IBBA_BALLAD_OBJECTS = []

  # starting the matching 
  print 'MATCHING'
  for i in range(TOTAL_NUM_SHEETMANIFS):
    # declare variables we need to store
    archid = 1
    estcid = ''
    shelfmark = ''
    imprint = ''
    license = 'NULL'
    authorimprint = 'NULL'
    pubstart = ''
    pubend = ''
    stdprintpub = 0
    collectioninfo = 0
    url = 'http://ballads.bodleian.ox.ac.uk/search/'

    estcid = str.split(str(sheet_manifs[i].estcid), ' ')[-1]

    # GET SHELFMARK
    if sheet_manifs[i].exemplar is not None:
      for sheet_item in sheet_items:
        # if exemplarof matches exemplar get shelfmark
        if sheet_item.exemplarOf == sheet_manifs[i].exemplar:
          # works! verified via print 
          shelfmark = str(sheet_item.shelfMark)
          # print shelfmark

    #  GET IMPRINT
    if sheet_manifs[i].composed_of is not None:
      # for each composed of in sheet manifestation
      for target_composed in sheet_manifs[i].composed_of:
        # if length is > 5, basically if it is UUID
        if len(target_composed) > 5:
          # search for the elements with same matching filename
          for element in elements:
            if element.filename == target_composed:
              # if match is found return the imprint or NULL
              if element.imprint is None: # fail
                imprint = 'NULL'
              else: # success
                imprint = element.imprint
                # print imprint 

    # GET PUBSTART AND PUBEND
    target_event = str.split(str(sheet_manifs[i].created_event), '/')[-1]
    for event in events:
      # if filename matches event return pub start and pub end
      if event.filename == target_event:
        if len(str(event.during_before)) != 4: #:$== ('' or ' '):
          pubend = 'NULL'
        else:
          pubend = event.during_before 
        if len(str(event.during_before)) != 4: # == ('' or ' '):
          pubstart = 'NULL'
        else:
          pubstart = event.during_after 
        # print pubend, pubstart

    # create new ballad object 
    new_ballad = ibba_ballad(archid, 
                              estcid,
                              shelfmark,
                              imprint, 
                              license,
                              authorimprint,
                              pubstart,
                              pubend,
                              stdprintpub,
                              collectioninfo,
                              url)

    # list of all the completed ibba_ballad_objects
    IBBA_BALLAD_OBJECTS.append(new_ballad)

    # progress check
    if i % 100 == 0:
      print str(i) + ' out of ' + str(TOTAL_NUM_SHEETMANIFS) + ' completed!'


  # finished matching, dump to pickled file 
  print 'DUMPING'
  f = open('ballads.p', 'wb')
  pickle.dump(IBBA_BALLAD_OBJECTS, f)
  f.close()


  
  # ###########################################################
  # db = db_connect()
  # cursor = db.cursor()

  # SQL_LINES_TO_EXECUTE = []
  # for obj in IBBA_BALLAD_OBJECTS:
  #   line = "(%s, '%s','%s','%s', %s, %s, %s, %s, %s, %s, '%s')" % (obj.ARCH_ID, obj.ESTCID, obj.Shelfmark, obj.Imprint, obj.License, obj.AuthorImprint, obj.PubStartDate, obj.PubEndDate, obj.StandardPrintPub, obj.CollectionInfo, obj.URL)
  #   insert_line = "INSERT INTO ibba_ballads( IBBA_BD_ARCH_ID, IBBA_BD_ESTCID, IBBA_BD_Shelfmark, IBBA_BD_Imprint, IBBA_BD_License, IBBA_BD_AuthorImprint, IBBA_BD_PubStartDate, IBBA_BD_PubEndDate, IBBA_BD_StandardPrintPub, IBBA_BD_CollectionInfo, IBBA_BD_URL) VALUES " + line + ";"
  #   SQL_LINES_TO_EXECUTE.append(insert_line);

  # print 'CREATING TABLE'
  # create_ibba_ballads = ('CREATE TABLE IF NOT EXISTS ibba_ballads (\
  # IBBA_BD_ID  BIGINT(20) NOT NULL AUTO_INCREMENT,\
  # IBBA_BD_ARCH_ID BIGINT(20),\
  # IBBA_BD_ESTCID VARCHAR(255),\
  # IBBA_BD_Shelfmark VARCHAR(255),\
  # IBBA_BD_Imprint VARCHAR(1100),\
  # IBBA_BD_License VARCHAR(255),\
  # IBBA_BD_AuthorImprint VARCHAR(255),\
  # IBBA_BD_PubStartDate INT(11),\
  # IBBA_BD_PubEndDate INT(11),\
  # IBBA_BD_StandardPrintPub BIGINT(20),\
  # IBBA_BD_CollectionInfo VARCHAR(255),\
  # IBBA_BD_URL VARCHAR(1100),\
  # PRIMARY KEY (IBBA_BD_ID)\
  # );')

  # cursor.execute(create_ibba_ballads)

  # print "Tables created..."
  # print 'FINISHED CREATING SQL LINES...'
  # print 'STARTING DATABASE INSERTION'
  # for line in SQL_LINES_TO_EXECUTE:
  #   print line
  #   cursor.execute(line)


  # # connect to ebbbasite mysql db
  # db = MySQLdb.connect(host="localhost",
  #                       user="root",
  #                       passwd="alexDSI123",
  #                       db="ebbasite")

  # # create cursor object, to execute queries
  # cur = db.cursor()

  # cur.execute('USE ebbasite;')

  # # show tables
  # # cur.execute('show tables;')
  # # for row in cur.fetchall():
  # #   print row[0]

  # cur.execute('select * from ibba_ballads;')
  # res = cur.fetchall()
  # print res


  # db.close()



