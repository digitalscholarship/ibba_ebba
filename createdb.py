import MySQLdb


def db_connect():
  '''
  function to connect to mysql database
  '''
  db = MySQLdb.connect(host="localhost",
                        user="root",
                        passwd="alexDSI123",
                        db="ibba_test")

  print 'DB READY FOR INSERT'
  return db

db = db_connect()
cursor = db.cursor()


print 'CREATING TABLE'
# schema is same as production relation schema
create_ibba_ballads = ('CREATE TABLE IF NOT EXISTS ibba_ballads (\
IBBA_BD_ID  BIGINT(20) NOT NULL AUTO_INCREMENT,\
IBBA_BD_ARCH_ID BIGINT(20),\
IBBA_BD_ESTCID VARCHAR(255),\
IBBA_BD_Shelfmark VARCHAR(255),\
IBBA_BD_Imprint VARCHAR(1100),\
IBBA_BD_License VARCHAR(255),\
IBBA_BD_AuthorImprint VARCHAR(255),\
IBBA_BD_PubStartDate INT(11),\
IBBA_BD_PubEndDate INT(11),\
IBBA_BD_StandardPrintPub BIGINT(20),\
IBBA_BD_CollectionInfo VARCHAR(255),\
IBBA_BD_URL VARCHAR(1100),\
PRIMARY KEY (IBBA_BD_ID)\
);')

# execute the create table command
cursor.execute(create_ibba_ballads)
db.commit()