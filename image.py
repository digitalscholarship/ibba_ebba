import os 
import untangle
import glob
import xmltodict

'''
for an image in sheetitemimage directory:
  1. image desription url 
  2. image type url 
  3. image sheetitem url 
'''
LIST_OF_SHEETIMAGE_OBJECTS = []
class sheetItemImage():

  def __init__(self, 
                description_url,
                type_url,
                image_url):
    self.description_url = description_url
    self.type_url = type_url
    self.image_url = image_url

# print image_files
def opendoc(filename):
  with open(filename) as df:
    try:
      doc = xmltodict.parse(df)
      # print filename
    except:
      print 'err'


# image description url 
def get_image_description_url(doc):
  image_description = doc['rdf:RDF']['rdf:Description']
  image_description_url = list(image_description.items())[0][-1]
  return image_description_url

# image type url 
def get_image_type_url(doc):
  image_type = doc['rdf:RDF']['rdf:Description']['rdf:type']
  image_type_url = list(image_type.items())[0][-1]
  return image_type_url


# image sheetItemImageURL
def get_image_url(doc):
  image_sheetItemImageURL = doc['rdf:RDF']['rdf:Description']['ballads:sheetItemImageURL']
  return image_sheetItemImageURL


image_path = "/Users/AlexSoong/Desktop/BalladsRDF/sheetitemimage/"
os.chdir(image_path)
image_dirs = glob.glob('*')

# print image_dirs
for image_dir in image_dirs:
  os.chdir(image_path + image_dir)
  images = glob.glob('*.rdf')
  for image in images:
    with open(image) as df:
      doc = xmltodict.parse(df.read())
    temp = sheetItemImage(get_image_description_url(doc),
                          get_image_type_url(doc),
                          get_image_url(doc))
    LIST_OF_SHEETIMAGE_OBJECTS.append(temp)


print len(LIST_OF_SHEETIMAGE_OBJECTS)
print LIST_OF_SHEETIMAGE_OBJECTS[0].description_url
print LIST_OF_SHEETIMAGE_OBJECTS[0].image_url
print LIST_OF_SHEETIMAGE_OBJECTS[0].type_url


# for img in image_files:
#   opendoc(img)

# with open(image_files[0]) as df:
#   doc = xmltodict.parse(df.read())  


