import pickle
import os 
import MySQLdb

def db_connect():
  '''
  function to connect to mysql database
  '''
  db = MySQLdb.connect(host="localhost",
                        user="root",
                        passwd="alexDSI123",
                        db="ibba_test")

  print 'DB READY FOR INSERT'
  return db


class ibba_ballad():
  ''' 
  the object that im going to insert into ibba_ballads table
  going to have a list of these objects
  iterate and insert into mysql 
  ''' 

  # in order to load a pickled object, you must have the constructor.
  def __init__(self,
                # BD_ID, AUTOINCREMENTING, DONT NEED TO INCLUDE
                ARCH_ID,
                ESTCID,
                Shelfmark,
                Imprint,
                License,
                AuthorImprint, 
                PubStartDate,
                PubEndDate,
                StandardPrintPub,
                CollectionInfo,
                URL):
    self.ARCH_ID = ARCH_ID
    self.ESTCID = ESTCID
    self.Shelfmark = Shelfmark
    self.Imprint = Imprint
    self.License = License
    self.AuthorImprint = AuthorImprint
    self.PubStartDate = PubStartDate
    self.PubEndDate = PubEndDate
    self.StandardPrintPub = StandardPrintPub
    self.CollectionInfo = CollectionInfo
    self.URL = URL

# open the pickled file
f = open('./pickled/nodate.p', 'rb')

# load the pickled file into ballads list
ballads = pickle.load(f)
f.close()

# connect to database, and initialize cursor
db = db_connect()
cursor = db.cursor()

# create the sql commands, based on ballads
SQL_LINES_TO_EXECUTE = []
for b in ballads:
  # for each ballad, there is a sql command 
  line = '(%s, "%s","%s","%s", %s, %s, %s, %s, %s, %s, "%s")' % (b.ARCH_ID, b.ESTCID, b.Shelfmark, b.Imprint, b.License, b.AuthorImprint, b.PubStartDate, b.PubEndDate, b.StandardPrintPub, b.CollectionInfo, b.URL)
  insert_line = "INSERT INTO ibba_ballads( IBBA_BD_ARCH_ID, IBBA_BD_ESTCID, IBBA_BD_Shelfmark, IBBA_BD_Imprint, IBBA_BD_License, IBBA_BD_AuthorImprint, IBBA_BD_PubStartDate, IBBA_BD_PubEndDate, IBBA_BD_StandardPrintPub, IBBA_BD_CollectionInfo, IBBA_BD_URL) VALUES " + line + ";"
  # store the seql command into a list of sql lines to execute by cursor
  SQL_LINES_TO_EXECUTE.append(insert_line);

# 
total = len(ballads)
goodcount = 0
backupcount = 0
inserted = 0
# for each sql command, cursor execute the sql 
for line in SQL_LINES_TO_EXECUTE:
  try:
    cursor.execute(line)
    goodcount += 1
    inserted += 1
  except:
    # hard code the date error to NULL
    line = '(%s, "%s","%s","%s", %s, %s, %s, %s, %s, %s, "%s")' % (b.ARCH_ID, b.ESTCID, b.Shelfmark, b.Imprint, b.License, b.AuthorImprint, 'NULL' , 'NULL', b.StandardPrintPub, b.CollectionInfo, b.URL)
    backupline = "INSERT INTO ibba_ballads( IBBA_BD_ARCH_ID, IBBA_BD_ESTCID, IBBA_BD_Shelfmark, IBBA_BD_Imprint, IBBA_BD_License, IBBA_BD_AuthorImprint, IBBA_BD_PubStartDate, IBBA_BD_PubEndDate, IBBA_BD_StandardPrintPub, IBBA_BD_CollectionInfo, IBBA_BD_URL) VALUES " + line + ";"
    cursor.execute(backupline)
    backupcount += 1
    inserted += 1

# for line in SQL_LINES_TO_EXECUTE:
#   cursor.execute(line)
# db.commit()
# print total
# print goodcount
# print backupcount
# print inserted
db.close()






