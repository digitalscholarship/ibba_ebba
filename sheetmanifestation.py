import os 
import untangle
import glob
import xmltodict

'''
returns all the information on sheetmanifestation 
'''

class sheetManifestation():
  def __init__(self,
                about_url,
                type_url,
                exemplar,
                num_ballads_onsheet,
                sheet_identifier,
                sheet_UUID,
                estcid,
                created_event,
                # text_manif_url,
                # imprint_url,
                composed_of):
    self.about_url = about_url
    self.type_url = type_url
    self.exemplar = exemplar
    self.num_ballads_onsheet = num_ballads_onsheet
    self.sheet_identifier = sheet_identifier
    self.sheet_UUID = sheet_UUID
    self.estcid = estcid
    self.created_event = created_event
    # self.text_manif_url = text_manif_url
    # self.imprint_url = imprint_url
    self.composed_of = composed_of

# sheet description url
# sheet_descrip = doc['rdf:RDF']['rdf:Description']
# sheet_items = list(sheet_descrip.items())

def get_about_url(doc):
  sheet_about = doc['rdf:RDF']['rdf:Description']
  sheet_about_url = list(sheet_about.items())[0][-1]
  return sheet_about_url

# sheet type url 
def get_type_url(doc):
  sheet_type = doc['rdf:RDF']['rdf:Description']['rdf:type']
  sheet_type_items_url = list(sheet_type.items())[0][-1]
  return sheet_type_items_url
# print sheet_type_items

# sheet exemplar url, and sheetitem number in sheetitem directory
def get_exemplar_sheetitem(doc):
  try:
    exemplar_url = doc['rdf:RDF']['rdf:Description']['ballads:exemplar'].items()[0][-1]
    return str.split(str(exemplar_url), '/')[-1]
  except:
    None
  else:
    return None
  # exemplar_items = list(exemplar)
  # exemplar_url = exemplar_items[0][-1]
  # return exemplar_url
  # sheet_ballad_exemplar = doc['rdf:RDF']['rdf:Description']['ballads:exemplar']
  # sheet_ballad_exemplar_url = sheet_ballad_exemplar.items()[0][-1]
  # return sheet_ballad_exemplar_url

# sheet_from_sheetitem = str.split(str(sheet_ballad_exemplar_url), '/')[-1]
# print sheet_ballad_exemplar_url, sheet_from_sheetitem

# num balladds on sheet
def get_num_ballads(doc):
  sheet_num_ballads_onsheet = doc['rdf:RDF']['rdf:Description']['ballads:numberBalladsOnSheet']
  return sheet_num_ballads_onsheet
# sheet ballad identifier

# sheet ballad identifiers
def get_sheet_id(doc):
  sheet_ballad_id_list = doc['rdf:RDF']['rdf:Description']['ballads:identifier']
  sheet_balladID = sheet_ballad_id_list[0]
  return sheet_balladID

def get_sheet_UUID(doc):
  sheet_ballad_id_list = doc['rdf:RDF']['rdf:Description']['ballads:identifier']
  sheet_balladUUID = str.split(str(sheet_ballad_id_list[1]), ':')[-1]
  return sheet_balladUUID

def get_estcid(doc):
  sheet_ballad_id_list = doc['rdf:RDF']['rdf:Description']['ballads:identifier']
  sheet_ballad_authorID = sheet_ballad_id_list[2]
  return sheet_ballad_authorID

# sheet event url, and event id
def get_manif_event_created(doc):
  try:
    sheet_event = doc['rdf:RDF']['rdf:Description']['ballads:sheetManifestationCreated']
    sheet_event_url = list(sheet_event.items())[0][-1]
    return sheet_event_url
  except:
    None
  else:
    return None
# sheet_event_UUID = str.split(str(sheet_event_url), '/')[-1]
# print sheet_event_UUID


# sheet ballad manifesttion url and ID
# def get_text_manif(doc):
#   try:
#     sheet_manif_composed_list = doc['rdf:RDF']['rdf:Description']['ballads:sheetManifestationComposedOf']
#     sheet_text_manif_url = list(sheet_manif_composed_list[0].items())[0][1]
#     return sheet_text_manif_url
#   except:
#     None
#   else:
#     return None
# sheet_text_manifID = str.split(str(sheet_text_manif_url), '/')[-1]

# def get_imprint_url(doc):  
#   try:
#     sheet_manif_composed_list = doc['rdf:RDF']['rdf:Description']['ballads:sheetManifestationComposedOf']
#     sheet_element_url = list(sheet_manif_composed_list[1].items())[0][1]
#     return sheet_element_url
#   except:
#     None
#   else:
#     return None
# sheet_elementUUID = str.split(str(sheet_element_url), '/')[-1]

def composed_list_helper(inlist):
  res = []
  for l in inlist:
    ID = str.split(str(l.items()[-1][-1]), '/') [-1]
    res.append(ID)
  return res

def get_composed_of(doc):
  try:
    composed_list = []
    sheet_composed_of = doc['rdf:RDF']['rdf:Description']['ballads:sheetManifestationComposedOf']
    # for sheet in sheet_composed_of:
    #   composed = sheet.items()[-1]
    #   composed_list.apped(composed)

    # if len(sheet_composed_of > 1):
      # for sc in sheet_composed_of:
        # composed_list =   composed_list.append(str.split(str(list(sc.items()))[-1][-1]), '/')[-1]
    return composed_list_helper(sheet_composed_of)
  except:
    None
  else:
    return None


def get_all():
  LIST_OF_SHEETMANIF_OBJECTS = []

  sheet_path = "/Users/AlexSoong/Desktop/BalladsRDF/sheetmanifestation/"
  os.chdir(sheet_path)
  sheet_dirs = glob.glob('*')

  for sheet_dir in sheet_dirs:
    try:
      os.chdir(sheet_path + sheet_dir)
    except:
      None
    sheets = glob.glob('*.rdf')
    for sheet in sheets:
      with open(sheet) as df:
        doc = xmltodict.parse(df.read())
      temp = sheetManifestation(get_about_url(doc),
                                get_type_url(doc),
                                get_exemplar_sheetitem(doc),
                                get_num_ballads(doc),
                                get_sheet_id(doc),
                                get_sheet_UUID(doc),
                                get_estcid(doc),
                                get_manif_event_created(doc),
                                # get_text_manif(doc),
                                # get_imprint_url(doc),
                                get_composed_of(doc))

      LIST_OF_SHEETMANIF_OBJECTS.append(temp)
  return LIST_OF_SHEETMANIF_OBJECTS



if __name__ == '__main__':

  mylist = get_all()

  for i in xrange(1000, 20000, 1000):
    print mylist[i].composed_of
# mylist[22345].composed_of

# for r in res:
#   print str.split(str(r.items()[-1][-1]), '/')[-1]
#   print '-----'
# print type(mylist[12345].composed_of.items())


# print len(LIST_OF_SHEETMANIF_OBJECTS)
# print LIST_OF_SHEETMANIF_OBJECTS[0].about_url
# print LIST_OF_SHEETMANIF_OBJECTS[0].type_url
# print LIST_OF_SHEETMANIF_OBJECTS[0].exemplar_url
# print LIST_OF_SHEETMANIF_OBJECTS[0].num_ballads_onsheet
# print LIST_OF_SHEETMANIF_OBJECTS[0].sheet_identifier
# print LIST_OF_SHEETMANIF_OBJECTS[0].sheet_UUID
# print LIST_OF_SHEETMANIF_OBJECTS[0].sheet_authorID
# print LIST_OF_SHEETMANIF_OBJECTS[0].created_event
# print LIST_OF_SHEETMANIF_OBJECTS[0].text_manif_url
# print LIST_OF_SHEETMANIF_OBJECTS[0].imprint_url


