import os 
import untangle
import glob
import xmltodict

'''
gets all the information for a textmanifestation
'''

LIST_OF_TEXTMANIF_OBJECTS = []
class balladTextManifestation():
  def __init__(self,
                about_url,
                type_url,
                identifier_UUID,
                authorID,
                num_ballads_onsheet,
                title_url, 
                text_body_url,
                partof_sheet_url):
    self.about_url =about_url
    self.type_url = type_url
    self.identifier_UUID = identifier_UUID
    self.authorID = authorID
    self.num_ballads_onsheet = num_ballads_onsheet
    self.title_url = title_url
    self.text_body_url = text_body_url
    self.partof_sheet_url = partof_sheet_url


# all of the text items for description
# text_descrip = doc['rdf:RDF']['rdf:Description']
# text_items = list(text_descrip.items())

# text description url
def get_about_url(doc):
  text_about = doc['rdf:RDF']['rdf:Description']
  text_about_url = list(text_about.items())[0][-1]
  return text_about_url

# print text type url 
def get_type_url(doc):
  text_type = doc['rdf:RDF']['rdf:Description']['rdf:type']
  text_type_url = list(text_type.items())[0][-1]
  return text_type_url

# lietral values 
# ballad UUID, and author ID
def get_identifier_UUID(doc):
  text_balladID_list = doc['rdf:RDF']['rdf:Description']['ballads:identifier']
  text_balladUUID = str.split(str(text_balladID_list[0]), ' ')[-1]
  return text_balladUUID

def get_authorID(doc):
  text_balladID_list = doc['rdf:RDF']['rdf:Description']['ballads:identifier']
  text_ballad_authorID = text_balladID_list[1]
  return text_ballad_authorID

# num ballads on sheet
def get_numballads_onsheet(doc):
  text_numballads_onsheet = doc['rdf:RDF']['rdf:Description']['ballads:numberOnSheet']
  return text_numballads_onsheet
# composed of title

def get_title_url(doc):
  try:
    text_composed_list = doc['rdf:RDF']['rdf:Description']['ballads:balladTextComposedOf']
    text_composed_title_url = list(text_composed_list[0].items())[0][1]
    return text_composed_title_url
  except:
    None
  else:
    return None
# text_composed_titleUUID = str.split(str(text_composed_title_url), '/')[-1]

def get_textbody_url(doc):
  try:
    text_composed_list = doc['rdf:RDF']['rdf:Description']['ballads:balladTextComposedOf']
    text_composed_textbody_url = list(text_composed_list[1].items())[0][1]
    return text_composed_textbody_url
  except:
    None
  else:
    return None
# text_composed_textbodyUUID = str.split(str(text_composed_textbody_url), '/')[-1]

# relation to sheet manifestation
def get_partof_sheet_url(doc):
  text_forms_sheetmanif = doc['rdf:RDF']['rdf:Description']['ballads:formsPartOfSheetManifestation']
  text_forms_sheetmanif_url = list(text_forms_sheetmanif.items())[0][1]
  return text_forms_sheetmanif_url
# print text_composed_textbody_url
# print text_composed_textbodyUUID


text_path = "/Users/AlexSoong/Desktop/BalladsRDF/balladtextmanifestation/"
os.chdir(text_path)
text_dirs = glob.glob('*')

for dir in text_dirs:
  os.chdir(text_path+dir)
  text_files = glob.glob('*.rdf')
  for text_file in text_files:
    with open(text_file) as df:
      doc = xmltodict.parse(df.read())

    temp = balladTextManifestation(get_about_url(doc),
                                    get_type_url(doc),
                                    get_identifier_UUID(doc),
                                    get_authorID(doc),
                                    get_numballads_onsheet(doc),
                                    get_title_url(doc),
                                    get_textbody_url(doc),
                                    get_partof_sheet_url(doc))

    LIST_OF_TEXTMANIF_OBJECTS.append(temp)


print len(LIST_OF_TEXTMANIF_OBJECTS)
print LIST_OF_TEXTMANIF_OBJECTS[31231].about_url
print LIST_OF_TEXTMANIF_OBJECTS[31231].type_url
print LIST_OF_TEXTMANIF_OBJECTS[31231].identifier_UUID
print LIST_OF_TEXTMANIF_OBJECTS[31231].authorID
print LIST_OF_TEXTMANIF_OBJECTS[31231].num_ballads_onsheet
print LIST_OF_TEXTMANIF_OBJECTS[31231].title_url
print LIST_OF_TEXTMANIF_OBJECTS[31231].text_body_url
print LIST_OF_TEXTMANIF_OBJECTS[31231].partof_sheet_url

# with open(text_manif_files[0]) as df:
#   doc = xmltodict.parse(df.read())

# image_description = doc['rdf:RDF']['rdf:Description']
#   image_description_url = list(image_description.items())[0][-1]
#   return image_description_url