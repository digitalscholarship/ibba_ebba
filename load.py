import pickle
import os 
import MySQLdb

def db_connect():
  '''
  function to connect to mysql database
  '''
  db = MySQLdb.connect(host="localhost",
                        user="root",
                        passwd="alexDSI123",
                        db="ibba_test")

  print 'DB READY FOR INSERT'
  return db

class ibba_ballad():
  ''' 
  the object that im going to insert into ibba_ballads table
  going to have a list of these bects
  iterate and insert into mysql 
  ''' 

  # in order to load a pickled object, you must have the constructor.
  def __init__(self,
                # BD_ID, AUTOINCREMENTING, DONT NEED TO INCLUDE
                ARCH_ID,
                ESTCID,
                Shelfmark,
                Imprint,
                License,
                AuthorImprint, 
                PubStartDate,
                PubEndDate,
                StandardPrintPub,
                CollectionInfo,
                URL):
    self.ARCH_ID = ARCH_ID
    self.ESTCID = ESTCID
    self.Shelfmark = Shelfmark
    self.Imprint = Imprint
    self.License = License
    self.AuthorImprint = AuthorImprint
    self.PubStartDate = PubStartDate
    self.PubEndDate = PubEndDate
    self.StandardPrintPub = StandardPrintPub
    self.CollectionInfo = CollectionInfo
    self.URL = URL

# open the pickled file
f = open('./pickled/nodate.p', 'rb')

# load the pickled file into ballads list
ballads = pickle.load(f)

# clise file 
f.close()

# connect to database, and initialize cursor
db = db_connect()
cursor = db.cursor()

# create the sql commands, based on ballads
SQL_LINES_TO_EXECUTE = []
for b in ballads:
  # for each ballad, there is a sql command 
  line = '(%s, "%s","%s","%s", %s, %s, %s, %s, %s, %s, "%s")' % (b.ARCH_ID, b.ESTCID, b.Shelfmark, b.Imprint, b.License, b.AuthorImprint, b.PubStartDate, b.PubEndDate, b.StandardPrintPub, b.CollectionInfo, b.URL)
  insert_line = "INSERT INTO ibba_ballads( IBBA_BD_ARCH_ID, IBBA_BD_ESTCID, IBBA_BD_Shelfmark, IBBA_BD_Imprint, IBBA_BD_License, IBBA_BD_AuthorImprint, IBBA_BD_PubStartDate, IBBA_BD_PubEndDate, IBBA_BD_StandardPrintPub, IBBA_BD_CollectionInfo, IBBA_BD_URL) VALUES " + line + ";"
  # store the seql command into a list of sql lines to execute by cursor
  SQL_LINES_TO_EXECUTE.append(insert_line);


# for TESTING
# if table does not exist, then create
print 'CREATING TABLE'
# schema is same as production relation schema
create_ibba_ballads = ('CREATE TABLE IF NOT EXISTS ibba_ballads (\
IBBA_BD_ID  BIGINT(20) NOT NULL AUTO_INCREMENT,\
IBBA_BD_ARCH_ID BIGINT(20),\
IBBA_BD_ESTCID VARCHAR(255),\
IBBA_BD_Shelfmark VARCHAR(255),\
IBBA_BD_Imprint VARCHAR(1100),\
IBBA_BD_License VARCHAR(255),\
IBBA_BD_AuthorImprint VARCHAR(255),\
IBBA_BD_PubStartDate INT(11),\
IBBA_BD_PubEndDate INT(11),\
IBBA_BD_StandardPrintPub BIGINT(20),\
IBBA_BD_CollectionInfo VARCHAR(255),\
IBBA_BD_URL VARCHAR(1100),\
PRIMARY KEY (IBBA_BD_ID)\
);')

# execute the create table command
cursor.execute(create_ibba_ballads)
db.commit()

print "Tables created..."
print 'FINISHED CREATING SQL LINES...'
print 'STARTING DATABASE INSERTION'



err = 0
# for each sql command, cursor execute the sql 
# for line in SQL_LINES_TO_EXECUTE:
#   try:
#     cursor.execute(line)
#   except:
#     print line
#     err += 1

# for line in SQL_LINES_TO_EXECUTE:
#   cursor.execute(line)
# db.commit()
db.close()



print 'NUMBER OF ERRORS: ' + str(err)



