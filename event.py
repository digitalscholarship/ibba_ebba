import os 
import untangle
import glob
import xmltodict
import open_weird
'''
For an entry in an event directory:
retrieve:
  1. event origin url 
  2. which collection event belongs to 
  3. sheet participant in sheetitem directory
'''


LIST_OF_EVENT_OBJECTS = []
class EVENT():
  def __init__(self, 
                description_url,
                sheetmanifestation_url,
                sheetitem_url,
                creater_agent_url, 
                location_url, 
                during_before, 
                during_after,
                creation_date,
                filename):
    self.description_url = description_url
    self.sheetmanifestation_url = sheetmanifestation_url
    self.sheetitem_url = sheetitem_url
    self.creater_agent_url = creater_agent_url
    self.location_url = location_url
    self.during_before = during_before
    self.during_after = during_after
    self.creation_date = creation_date
    self.filename = filename


# print event_files
def opendoc(filename):
  with open(filename) as df:
    try:
      doc = xmltodict.parse(df.read())
      return doc
    except OSError as err:
      print("OS error: {0}".format(err))


# print doc
# about

list_about_url = []
def get_about_url(doc):
  try:
    doc_description = doc['rdf:RDF']['rdf:Description']
    items = list(doc_description.items())
    about_url = items[0][1]
    return about_url
  except:
    return None
  # list_about_url.append(about_url)
# strsplit(str(about_url, '/'))

# not sure if we need this one
# doc_type = doc['rdf:RDF']['rdf:Description']['rdf:type']
# doc_type_items = list(doc_type.items())
def get_sheetitem_url(doc):
  try:
    doc_sheet = doc['rdf:RDF']['rdf:Description']['ballads:hasSheetItemParticipant']
    doc_sheet_items = list(doc_sheet.items())
    sheetitem_url = doc_sheet_items[0][-1]
    return sheetitem_url
  except:
    return None

def get_event_origin_collection(doc):
  try:
    # information regarding what collection the event is from
    doc_hasCollection = doc['rdf:RDF']['rdf:Description']['ballads:hasCollection']
    doc_collection_items = list(doc_hasCollection.items())
    event_collection_url = doc_collection_items[0][-1]
    # doc_origin_collection_number = str.split(str(doc_collection_items[0][1]), '/')[-1]
    return doc_origin_collection_url
    # list_event_collection_info.append([event_collection_url, doc_origin_collection_number])
  except:
    return None

def get_sheetmanifestation(doc):
  try:
    doc_hasSheetItemParticipant = doc['rdf:RDF']['rdf:Description']['ballads:hasSheetManifestationParticipant']
    doc_items = list(doc_hasSheetItemParticipant.items())
    doc_sheetmanif_url = doc_items[0][-1]
    return doc_sheetmanif_url
  except:
    return None


def get_event_sheetitem_participant(doc):
  try:
    doc_hasSheetItemParticipant = doc['rdf:RDF']['rdf:Description']['ballads:hasSheetItemParticipant']
    doc_sheetItem_items = list(doc_hasSheetItemParticipant.items())
    doc_sheetitem_url = doc_sheetItem_items[0][-1]
    return doc_sheetitem_url
    # doc_has_sheet_number = str.split(str(doc_sheetItem_items[0][1]), '/') [-1]
    # list_event_sheetitem_info.append([doc_sheetitem_url, doc_has_sheet_number])
    # print doc_has_sheet
  except:
    return None

def get_during_before(doc):
  try:
    return doc['rdf:RDF']['rdf:Description']['ballads:occurredDuringOrBeforeYear']
  except:
    return None

def get_during_after(doc):
  try:
    return doc['rdf:RDF']['rdf:Description']['ballads:occurredDuringorAfterYear']
  except:
    return None

def get_creation_date(doc):
  try:
    return doc['rdf:RDF']['rdf:Description']['ballads:creationDateText']
  except:
    return None

# test_file = '/Users/AlexSoong/desktop/BalladsRDF/test_event/event/00/0014d521-5836-4463-acbc-e442ad719f43.rdf'
# with open(test_file) as df:
#   doc = xmltodict.parse(df.read())

# print get_creation_date(doc)


def get_all():
  # need to make this a function and pass all the directories inside events 
  events_path = "/Users/AlexSoong/Desktop/BalladsRDF/event/"
  os.chdir(events_path)
  #event_files has all the .rdf files of all the events.
  event_dirs = glob.glob("*")

  for event_dir in event_dirs:
    os.chdir(events_path + event_dir)
    events = glob.glob('*.rdf')
    for event in events:
      with open(event) as df:
        doc = xmltodict.parse(df.read())
      temp = EVENT(get_about_url(doc),
                          get_event_origin_collection(doc),
                          get_sheetmanifestation(doc),
                          get_sheetitem_url(doc),
                          get_event_sheetitem_participant(doc),
                          get_during_before(doc),
                          get_during_after(doc),
                          get_creation_date(doc),
                          str.split(str(event),'.')[0])
      LIST_OF_EVENT_OBJECTS.append(temp)
  return LIST_OF_EVENT_OBJECTS


# print len(LIST_OF_EVENT_OBJECTS)
# print LIST_OF_EVENT_OBJECTS[9].description_url
# print LIST_OF_EVENT_OBJECTS[9].creater_agent_url
# print LIST_OF_EVENT_OBJECTS[9].location_url
# print LIST_OF_EVENT_OBJECTS[9].sheetitem_url
# print LIST_OF_EVENT_OBJECTS[9].during_before
# print LIST_OF_EVENT_OBJECTS[9].during_after
# print LIST_OF_EVENT_OBJECTS[9].creation_date
# print LIST_OF_EVENT_OBJECTS[9].sheetmanifestation_url











