import os 
import untangle
import glob
import xmltodict

'''
for a sheet item in a sheetitem directory returns :
  1. sheet's origin url 
  2. sheet type url
  3. sheet's sheet manifeestation url, and sheetmanifestation number 
  4. sheet's event url, and event number
  5. sheet's image url, and image id 
  6. sheet's shelfMark 
  7. sheet's ballad identifier UUID
'''
class sheetItem():
  def __init__(self, 
                origin_url, 
                type_url, 
                exemplarOf, 
                origin_collection, 
                image_url, 
                shelfMark, 
                UUID,
                filename):
    # initialize class variables
    self.origin_url = origin_url
    self.type_url = type_url
    self.exemplarOf = exemplarOf
    self.origin_collection = origin_collection
    self.image_url = image_url
    self.shelfMark = shelfMark
    self.UUID = UUID
    self.filename = filename
# end of sheetItem class

# function to return origin urls 
def get_sheet_origin_url(doc):
  sheet_description = doc['rdf:RDF']['rdf:Description']
  sheet_origin_url = list(sheet_description.items())[0][-1]
  return sheet_origin_url
  # list_sheet_origin_url.append(sheet_origin_url)


# sheet type url 
def get_sheet_type_url(doc):
  sheet_type = doc['rdf:RDF']['rdf:Description']['rdf:type']
  sheet_type_items = list(sheet_type.items())
  sheet_type_url = sheet_type_items[0][-1]
  return sheet_type_url

# from sheet manifestation directory
# sheet exemplarOf, sheetmanifestation url, and sheet manifestation number
def get_exemplar(doc):
  sheet_exemplarOf = doc['rdf:RDF']['rdf:Description']['ballads:exemplarOf']
  sheet_exemplarOf_url = list(sheet_exemplarOf.items())[0][-1]
  sheet_exemplar = str.split(str(sheet_exemplarOf_url), '/')[-1]
  return sheet_exemplar
# print sheet_sheetmanifestation


# get sheet event url, event uuid, from event directory
def get_sheet_collection(doc):
  sheet_collection = doc['rdf:RDF']['rdf:Description']['ballads:partOfCollection']
  sheet_event_url = list(sheet_collection.items())[0][-1]
  sheet_event_UUID = str.split(str(sheet_event_url),'/')[-1]
  return [sheet_event_url, sheet_event_UUID]
  # print sheet_event_url, sheet_event_UUID


# sheet image url, and image id from sheetitemimage directory
def get_image_url(doc):
  try:
    sheet_image = doc['rdf:RDF']['rdf:Description']['ballads:hasDigitalImage']
    sheet_image_url = list(sheet_image.items())[0][-1]
    sheet_image_id = str.split(str(sheet_image_url), '/')[-1]
    return sheet_image_url
  except:
    None
  else:
    return None

# ballads shelfMark
def get_shelfMark(doc):
  sheet_shelfMark = doc['rdf:RDF']['rdf:Description']['ballads:shelfMark']
  return sheet_shelfMark
# print sheet_shelfMark

# ballads UUID
def get_UUID(doc):
  sheet_UUID = str.split(str(doc['rdf:RDF']['rdf:Description']['ballads:identifier']), ' ')[-1]
  return sheet_UUID


#############################################
#############################################
# test_dir = '/Users/AlexSoong/Desktop/BalladsRDF/sheetitem/0/'
# os.chdir(test_dir)
# test_items = glob.glob('*.rdf')
# print test_items


# for test in test_items:
#   with open(test) as df:
#     doc = xmltodict.parse(df.read())
#   print test_items.index(test)
#   print get_image_url(doc)




def get_all():
  # path to all sheetitem directories
  sheetitem_path = "/Users/AlexSoong/Desktop/BalladsRDF/sheetitem/"
  os.chdir(sheetitem_path)
  # list of all the sheet item directories
  sheetitem_dirs = glob.glob('*')

  # list_sheet_items = []
  LIST_OF_SHEETITEM_OBJECTS = []

  # flatten = lambda l: [item for sublist in l for item in sublist]


  # print sheetitem_dirs
  # list of lists 
  # each sublist is a list of sheet items within that directory
  for dir in sheetitem_dirs:
    os.chdir(sheetitem_path + dir)
    sheet_items = glob.glob('*.rdf')
    for item in sheet_items:
      with open(item) as df:
        doc = xmltodict.parse(df.read())
      temp = sheetItem(get_sheet_origin_url(doc),
                        get_sheet_type_url(doc),
                        get_exemplar(doc),
                        get_sheet_collection(doc),
                        get_image_url(doc),
                        get_shelfMark(doc),
                        get_UUID(doc),
                        str.split(str(item), '.')[0])
      LIST_OF_SHEETITEM_OBJECTS.append(temp)
  return LIST_OF_SHEETITEM_OBJECTS

# print len(LIST_OF_SHEETITEM_OBJECTS)
# print LIST_OF_SHEETITEM_OBJECTS[0].origin_url
# print LIST_OF_SHEETITEM_OBJECTS[0].type_url
# print LIST_OF_SHEETITEM_OBJECTS[0].exemplarOf
# print LIST_OF_SHEETITEM_OBJECTS[0].origin_collection
# print LIST_OF_SHEETITEM_OBJECTS[0].image_url
# print LIST_OF_SHEETITEM_OBJECTS[0].shelfMark
# print LIST_OF_SHEETITEM_OBJECTS[0].UUID

