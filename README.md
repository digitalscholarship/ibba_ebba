# README #

This README will be blog style of what I'm working on currently, and how I'm doing it.


### What is this repository for? ###
I am parsing through the ibba files, and connecting the pieces. 
The end goal is to insert all the ibba ballads into ebbasite MySQL database

### To Run ###
Please make sure all dependent python libraries are installed. 
Please change db_connect() in insert.py or load.py to your own mysql login name and password.

### Data
The data I parsed through is zipped up in BalladsRDF.zip, it is a rather large folder, so it will take some time to zip/unzip

## Parsing XML files 
- ballad.py
- sheetitem.py
- sheetmanifestation.py
- image.py
- element.py 
- event.py

## pickled files
- ballad_objects.p
- ballads.p 
- elements.p
- events.p
- nodate.p
- sheetitems.p 
- sheets.p 

## creating a testing DB
- test_db.py

## inserting into mySQL
- insert.py 
- load.py

## error handling
- open_weird.py
- empty_date.py


### How do I get set up? ###

I am using python 2.7.13 to do this task 
I am using pip 9.0.1 to install 3rd party python libraries 
I am using mySQL 5.7.18 as the command line interface to a mySQL db

#### Python Libraries ####
- pickle 
- MySQLdb 
- xmltodict 
- glob 
- untangle 
- fileinput   
- sys
- os 



### Who do I talk to? ###
For any questions, email me: asoong@ucdavis.edu