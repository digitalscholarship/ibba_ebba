import os 
import untangle
import glob
import xmltodict
'''
get info from elements
'''

class element_item():
  def __init__(self,
                title, 
                reference,
                firstline,
                imprint, 
                tune,
                filename):
    self.title = title
    self.reference = reference
    self.firstline = firstline 
    self.imprint = imprint
    self.tune = tune
    self.filename = filename

def get_element_title(doc):
  try:
    element_title = doc['rdf:RDF']['rdf:Description']['ballads:title']
    return element_title
  except:
    None
  else:
    return None 

def get_element_reference(doc):
  # returns an array of references
  try:
    element_reference = doc['rdf:RDF']['rdf:Description']['ballads:reference']
    return element_reference
  except:
    None
  else:
    return None

def get_element_firstline(doc):
  try:
    element_firstline = doc['rdf:RDF']['rdf:Description']['ballads:firstLine']
    return element_firstline
  except:
    None
  else:
    return None

def get_element_imprint(doc):
  try:
    element_imprint = doc['rdf:RDF']['rdf:Description']['ballads:imprint']
    return element_imprint
  except:
    None
  else:
    return None

def get_element_tune(doc):
  try:
    element_tune = doc['rdf:RDF']['rdf:Description']['ballads:tuneName']
    return element_tune
  except:
    None
  else:
    return None

def get_all():
  LIST_OF_ELEMENTS = []
  element_path = "/Users/AlexSoong/Desktop/BalladsRDF/element/"
  # test = '/0039af39-f6f9-46f3-b2d3-37b589f0817c.rdf'
  os.chdir(element_path)
  element_dirs = glob.glob('*')

  for eldir in element_dirs:
    os.chdir(element_path + eldir)
    elements = glob.glob('*.rdf')
    for element in elements:
      try:
        with open(element) as df:
          doc = xmltodict.parse(df.read())
      except:
        None
        # print "XPATH ERROR"
      temp = element_item(get_element_title(doc),
                    get_element_reference(doc),
                    get_element_firstline(doc),
                    get_element_imprint(doc),
                    get_element_tune(doc),
                    str(str.split(element, '.')[0]) )
      LIST_OF_ELEMENTS.append(temp)

  return LIST_OF_ELEMENTS

    # element_files = glob.glob('*.rdf')
    # for element in element_files:
    #   # try:
    #   with open(element) as df:
    #     doc = xmltodict.parse(df.read())
    #   temp = element(get_element_title(doc),
    #                 get_element_reference(doc),
    #                 get_element_firstline(doc),
    #                 get_element_imprint(doc),
    #                 get_element_tune(doc) )
    #   LIST_OF_ELEMENTS.append(temp)
          # print element
          # print get_element_reference(doc)
      # except:
        # None

      # temp = element(get_element_title(doc),
      #                 get_element_reference(doc),
      #                 get_element_firstline(doc),
      #                 get_element_imprint(doc),
      #                 get_element_tune(doc) )
      # LIST_OF_ELEMENTS.append(temp)

  # return LIST_OF_ELEMENTS









# element_descrip_list = list(doc['rdf:RDF']['rdf:Description'].items())
# element_descrip_url = element_descrip_list[0][-1]
# element_type_url = list(element_descrip_list[1][-1].items())[0][-1]
# element_textmanif_url = list(doc['rdf:RDF']['rdf:Description']['ballads:formsPartOfBalladText'].items())[0][-1]
# elementUUID = str.split(str(doc['rdf:RDF']['rdf:Description']['ballads:identifier']))[-1]
